package lagerverwaltung.backend;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.util.LinkedList;

import lagerverwaltung.exceptions.DatenbankUnerreichbarException;
import lagerverwaltung.exceptions.LagerVollException;
import lagerverwaltung.exceptions.LagerplatzBelegtException;
import lagerverwaltung.exceptions.LagerplatzOutOfBounceException;
//import lagerverwaltung.exceptions.ProductNotFoundException;

import lagerverwaltung.middleware.Logger;


//LOKALE VERWENDBARKEIT DER DATENBANK:
//1. herunterladen: https://bitbucket.org/xerial/sqlite-jdbc/downloads
//2. Eclipse: Rechtsklick auf das Projekt
//3. Build-Path -> Configure Build-Path
//4. Links: Java-Build-Path -> Reiter Libaries
//5. Rechts: Add External Jars... -> Heruntergeladene Datei auswaehlen
//6. OK und Fenster schliessen



/**
 * Diese Klasse stellt die Schnittstelle zur Kommunikation mit der Datenbank dar.
 * Sie wird gefuellt mit den konrekten Interface-SQL-Methoden.
 * @author Marco
 */
public class DBWrapper implements DBConnect {

	private Connection connection = null;
	private Statement statement = null;
	
	/**
	 * Der SQLite Error Code für [SQLITE_CONSTRAINT_UNIQUE]  A UNIQUE constraint failed (UNIQUE constraint failed: <table>)
	 */
	int uniqueconstraintfailed = 19;
	
	/*
	 * database connection will be automatically create by object constructor.
	 */
	public DBWrapper() throws SQLException, DatenbankUnerreichbarException {
		Logger.writeLog("Backend: DBWrapper Konstruktor aufgerufen");
		connect();
		initialiseDB();
	}
	
	public boolean connect() {
		Logger.writeLog("Backend: connect aufgerufen");
		try{
			/**
			 * Zuerst laden wir den JDBC-Treiber, danach sagen wir wo die DB im Verzeichnis liegt.
			 * Danach erzeugen wir uns ein Statement worueber die Query geschickt werden.
			 */
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:bin/lagerverwaltung/backend/sample.db");
			statement = connection.createStatement();
			
			/**
			 * Wir wollen bei zukuenftigen Querys nicht ewig warten. Nach 10 Sekunden wird abgebrochen!
			 */
			statement.setQueryTimeout(5);
			Logger.writeLog("Rueckgabe: " + true);
			return true;
		}catch(SQLTimeoutException tout){
			Logger.writeLog("SQLTimeOutException gefangen");
			Logger.writeLog(tout);
			return false;
		}catch(SQLException sqle){
			Logger.writeLog("SQLException gefangen");
			Logger.writeLog(sqle);
			return false;
		}catch(ClassNotFoundException cnfe){
			Logger.writeLog("ClassNotFoundException gefangen");
			Logger.writeLog(cnfe);
			return false;
		}
	}
	
	/**
	 * Diese Methode ueberprueft ob die Datenbanktabellen vollstaendig vorhanden sind.
	 * Sind sie nicht vorhanden, werden alle Datenbanktabellen neu (und damit leer) angelegt.
	 * @return boolean True: DB-Initialisierung erfolgreich False: DB-Inititialisierung fehlgeschlagen
	 */
	public boolean initialiseDB() throws SQLException, DatenbankUnerreichbarException {
		Logger.writeLog("Backend: initialiseDB aufgerufen");
		try{
			boolean b1 = checkIfTableExist("parts");
			boolean b2 = checkIfTableExist("storage");
			
			if (b1 == true && b2 == true){
				//alles ok, nichts weiter zu machen
			}
			
			if (b1 == true && b2 == false) {
				//parts existiert, aber Lagerplaetze weg
				//Wenn wir an dieser Stelle die Tabellen neu schreiben, verschwinden auch die
				//wichtigen Packetplaetze... Exception werfen?
			}
			if (b1 == false && b2 == true){
				//Wichtige Packetplaetze verloren. Die Datenbanktabellen koennen neu angelegt werden
				create_DatabaseTables();
			}
			if (b1 == false && b2 == false){
				//Nichts vorhanden. Datenbanktabellen neu schreiben
				create_DatabaseTables();
			}
			
			return true;
		}catch(DatenbankUnerreichbarException due){
			Logger.writeLog("DatenbankUnerreichbarException gefangen");
			Logger.writeLog(due);
			return false;
		}catch(SQLTimeoutException tout){
			Logger.writeLog("SQLTimeOutException gefangen");
			Logger.writeLog(tout);
			return false;
		}catch(SQLException sqle){
			Logger.writeLog("SQLException gefangen");
			Logger.writeLog(sqle);
			return false;
		}
	}
	
	/**
	 * Diese Methode erzeugt die DB-Tabellen um ueberhaupt etwas in eine Datenbank speichern zu koennen.
	 * @throws DatenbankUnerreichbarException wenn Datenbank nicht erreichbar
	 * @throws SQLException bei sonsigen Exceptions mit der Datenbank
	 */
	private void create_DatabaseTables() throws SQLException, DatenbankUnerreichbarException{
		Logger.writeLog("Backend: create_DatabaseTables aufgerufen");
    	try {
    		/**
    		 * Das ist die Stelle, an der die bisherige gesamte DB-geloescht wird und neue leere Tabellen 
    		 * erzeugt werden.
    		 */
			statement.executeUpdate("DROP TABLE IF EXISTS storage");
			statement.executeUpdate("DROP TABLE IF EXISTS parts");
			/**
			 * Achtung, das folgende parts.number ist bewusst NICHT UNIQUE!
			 * Wie sagte schon Herr ten Hagen: "Wenn der Kunde sagt, etwas ist eindeutig, dann ist es nicht eindeutig!"
			 * Hingegen ist der Lagerplatz (storage.place) tatsaechlich physisch auf 1 begrenzt, verdient also unique.
			 * Es wurde sich mehrfach darauf geeinigt, dass Packetnummern nur einmal zur gleichen Zeit vorkommen.
			 * Es kann aber ein Packet gelöscht werden und danach eines mit der gleichen PID wieder eingefügt. Das geht.
			 */
			statement.executeUpdate("CREATE TABLE storage (id INTEGER PRIMARY KEY , place INT UNIQUE)");
			statement.executeUpdate("CREATE TABLE parts (id INTEGER PRIMARY KEY , number TEXT UNIQUE NOT NULL, storage_place INT UNIQUE NOT NULL, FOREIGN KEY(storage_place) REFERENCES storage(id))");
	    	
			Logger.writeLog("Tabellenerzeugung erfolgreich");
	    	
			/**
			 * Die Tabellen stehen jetzt. Zeit um alle Lagerplaetze einzurichten.
			 */
	    	this.createStoragePlaces();
	    	
		}catch(SQLTimeoutException tout) {
			Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
			Logger.writeLog(new DatenbankUnerreichbarException(tout.getMessage()));
			throw new DatenbankUnerreichbarException(tout.getMessage());
		}catch(SQLException sqle){
			Logger.writeLog("SQLException gefangen und weitergeworfen");
			Logger.writeLog(sqle);
			throw sqle;
		}
    }
	
	private boolean checkIfTableExist(String table) throws SQLException, DatenbankUnerreichbarException{
		Logger.writeLog("Backend: checkIfTableExist aufgerufen");
		Logger.writeLog("Parameter table: " + table);
		boolean bool = false;
		ResultSet dbresult;
		String dbruckgabe = "";
		try{
			dbresult = statement.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + table + "';");
			while(dbresult.next()){
				dbruckgabe = dbresult.getString("name");
			}
		}catch(SQLTimeoutException tout){
			Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
			Logger.writeLog(new DatenbankUnerreichbarException(tout.getMessage()));
			throw new DatenbankUnerreichbarException(tout.getMessage());
		}catch(SQLException sqle){
			Logger.writeLog("SQLException gefangen und weitergeworfen");
			Logger.writeLog(sqle);
			throw sqle;
		}
		if (table.equals(dbruckgabe)){
			Logger.writeLog("Parameter: " + table + " DB-Ruckgabe: " + dbruckgabe);
			Logger.writeLog("TABELLE " + table + " EXISTIERT");
			bool = true;
		}else{
			Logger.writeLog("Parameter: " + table + " DB-Ruckgabe: " + dbruckgabe);
			Logger.writeLog("TABELLE " + table + " EXISTIERT NICHT");
			bool = false;
		}
		Logger.writeLog("Rueckgabe: " + bool);
		return bool;
	}
	
	 /**
     * Diese Methode fuellt die Datenbank mit den verfuegbaren Lagerplaetzen. Zurzeit: 0 bis 100
     * @throws DatenbankUnerreichbarException wenn Datenbank nicht erreichbar
     * @throws SQLException bei sonsigen Exceptions mit der Datenbank
     */
    private void createStoragePlaces() throws SQLException, DatenbankUnerreichbarException{
    	Logger.writeLog("Backend: createStoragePlaces aufgerufen");
    	/**
    	 * Wir richten die noetigen Lagerplaetze ein. Also 1 bis einschliesslich 100.
    	 * An Backend: Auto_Increment ist nicht noetig. SQLite macht das von selbst, sofern es nicht in InsertInto etc.
	     * mitangegeben wird. Quelle: http://sqlite.org/autoinc.html
	     */
    	Logger.writeLog("Einrichtung Lagerplaetze");
    	try{
    		for (int i = 1; i <= 100; i++){
    			statement.executeUpdate("INSERT INTO storage (place) VALUES (" + i + ")");
    		}
    		Logger.writeLog("Lagerplaetze eingerichtet");
    		
    	}catch(SQLTimeoutException tout){
    		Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
			Logger.writeLog(new DatenbankUnerreichbarException(tout.getMessage()));
			throw new DatenbankUnerreichbarException(tout.getMessage());
    	}catch(SQLException sqle){
    		Logger.writeLog("SQLException gefangen und weitergeworfen");
    		Logger.writeLog(sqle);
    		throw sqle;
    	}	
    	
    	
    	/**
    	 * Es wurde eine bessere(?) Methode gefunden als 100x executeUpdate auszufuehren und damit
    	 * 100x zu comitten und 100x eine Datei zu speichern.
    	 * Wir verwenden ein prepared Statement.
    	 */
    	
    	/** 
    	 * In meiner Quelle (ein 1500-Seiten Java Buch ^^) wurde exakt das Beispiel vorgestellt, wie
    	 * wir es bisher gemacht haben: per Statement.execute(). Darin ist die Rede, dass es
    	 * mit dem Prepared Statement 10x schneller geht. Das sollten wir mal antesten...
    	 */
    	/*
    	PreparedStatement ps = connection.prepareStatement("INSERT INTO storage (place) VALUES(?)");
    	for( int i = 0; i< 100; i++){
    		ps.setInt(1, i);
    		ps.execute();
    	}
    	*/
    	
    	
    }
	
    @Override
    public LinkedList<String> getNextBestPlace() throws SQLException, DatenbankUnerreichbarException, LagerVollException{
    	Logger.writeLog("Backend: getNextBestPlace aufgerufen");
    	
    	ResultSet dbresult = null;
		LinkedList<String> bestplace = new LinkedList<>();
			/**
			 * SQLite unterstuetzt nur Left-Outer-Join, kein RIGHT- und FULL-OUTER JOIN
			 * Einen Inner Join koennen wir nicht gebrauchen, da dann keine Nulls mitgejoint werden.
			 * Wir testen aber direkt auf is null.
			 * Der Vergleichsquery lautet: SELECT storage.id, storage.place, parts.number, parts.storage_place FROM storage 
			 *								JOIN parts on storage.id = parts.storage_place
			 * und dieser zieht keine NULLs mit. LEFT-OUTER-JOIN schon.
			 */
			String bestplace_string = "";
		try{
			dbresult = statement.executeQuery("SELECT min(storage.place) FROM storage LEFT OUTER JOIN parts on storage.id = parts.storage_place WHERE parts.storage_place IS NULL");
			while(dbresult.next()){
				bestplace_string = Integer.toString((dbresult.getInt("min(storage.place)")));
			}
			
			bestplace.add(bestplace_string);
			Logger.writeLog("Rueckgabe: " + bestplace);
			if (bestplace.size() == 0){
				Logger.writeLog("LagervollException geworfen");
				LagerVollException lve = new LagerVollException();
				Logger.writeLog(lve);
				throw lve;
			}
		}catch(SQLTimeoutException tout){
			Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
			Logger.writeLog(new DatenbankUnerreichbarException(tout.getMessage()));
			throw new DatenbankUnerreichbarException(tout.getMessage());
		}catch(SQLException sqle){
			Logger.writeLog("SQLException gefangen und weitergeworfen");
			Logger.writeLog(sqle);
			throw sqle;
		}
		Logger.writeLog("Rueckgabe: " + bestplace);
		return bestplace;
    }

    
    /*
     * Autor: Christian
     * Neue Methode um testUnused zusetzten, Kopie von getAllPlaces() mit geaenderter SQL
     * Wie die Methode dann heißt muss noch geklärt werden
     */
    @Override
    public LinkedList<String> getAllUsedPlaces() throws SQLException, DatenbankUnerreichbarException{
    	Logger.writeLog("Backend: getAllUsedPlaces aufgerufen");
    	
    	
    	LinkedList<String> list = new LinkedList<>();
    	ResultSet dbresult = null;

   		try {
			dbresult = statement.executeQuery("SELECT place FROM storage WHERE place IN (SELECT storage_place AS place FROM parts)");
   			while(dbresult.next()) {
   				list.add(Integer.toString(dbresult.getInt("place")));
   			}
   		}catch(SQLTimeoutException tout){
   			Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
			Logger.writeLog(new DatenbankUnerreichbarException(tout.getMessage()));
			throw new DatenbankUnerreichbarException(tout.getMessage());
   		}catch(SQLException sqle){
   			Logger.writeLog("SQLException gefangen und weitergeworfen");
   			Logger.writeLog(sqle);
   			throw sqle;
   		}
   		Logger.writeLog("Rueckgabe: " + list);
        return list;
    }
    
    private LinkedList<String> getAllUnusedPlaces() throws SQLException, DatenbankUnerreichbarException{
    	Logger.writeLog("Backend: getAllUnusedPlaces aufgerufen");
    	
    	
    	LinkedList<String> list = new LinkedList<>();
    	ResultSet dbresult = null;

   		try {
			dbresult = statement.executeQuery("SELECT place FROM storage WHERE place NOT IN (SELECT storage_place AS place FROM parts)");
   			while(dbresult.next()) {
   				list.add(Integer.toString(dbresult.getInt("place")));
   			}
   		}catch(SQLTimeoutException tout){
   			Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
			Logger.writeLog(new DatenbankUnerreichbarException(tout.getMessage()));
			throw new DatenbankUnerreichbarException(tout.getMessage());
   		}catch(SQLException sqle){
   			Logger.writeLog("SQLException gefangen und weitergeworfen");
   			Logger.writeLog(sqle);
   			throw sqle;
   		}
   		Logger.writeLog("Rueckgabe: " + list);
        return list;
    }
    
    

    @Override
    public LinkedList<String> getAllPlaces() throws SQLException, DatenbankUnerreichbarException{
    	Logger.writeLog("Backend: getAllPlaces aufgerufen");
    	
    	
    	LinkedList<String> list = new LinkedList<>();
    	ResultSet dbresult = null;

   		try {
			dbresult = statement.executeQuery("SELECT place FROM storage");
   			while(dbresult.next()) {
   				list.add(Integer.toString(dbresult.getInt("place")));
   			}
   		}catch(SQLTimeoutException tout){
   			Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
			Logger.writeLog(new DatenbankUnerreichbarException(tout.getMessage()));
			throw new DatenbankUnerreichbarException(tout.getMessage());
   		}catch(SQLException sqle){
   			Logger.writeLog("SQLException gefangen und weitergeworfen");
   			Logger.writeLog(sqle);
   			throw sqle;
   		}
   		Logger.writeLog("Rueckgabe: " + list);
        return list;
    }

    @Override
    public LinkedList<String> getAllPIDs() throws SQLException, DatenbankUnerreichbarException {
    	Logger.writeLog("Backend: getAllPIDs aufgerufen");
    	LinkedList<String> list = new LinkedList<>();
    	ResultSet dbresult = null;

   		try {
			dbresult = statement.executeQuery("SELECT number FROM parts");
			while(dbresult.next()) {
				list.add(dbresult.getString("number"));
			}
   		}catch(SQLTimeoutException tout){
   			Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
			Logger.writeLog(new DatenbankUnerreichbarException(tout.getMessage()));
			throw new DatenbankUnerreichbarException(tout.getMessage());
		}catch(SQLException sqle) {
			Logger.writeLog("SQLException gefangen und weitergeworfen");
			Logger.writeLog(sqle);
			throw sqle;
		}
   		Logger.writeLog("Rückgabe LinkedList: " + list);
        return list;
    }

    @Override
    public String getPID(String place) throws SQLException, DatenbankUnerreichbarException, LagerplatzOutOfBounceException {
    	Logger.writeLog("Backend: getPID aufgerufen");
    	Logger.writeLog("Parameter place: " + place);
    	String number = "";
    	ResultSet dbresult = null;

    	try{
    	//Test auf LagerplatzOutOfBounce
    		if (testLagerplatzExistiert(place)){
    	//Test bestanden: Lagerplatz existiert. Query kann ausgeführt werden
				dbresult = statement.executeQuery(String.format("SELECT number FROM parts WHERE storage_place = (SELECT id FROM storage WHERE place =%s)", place));
				while (dbresult.next()){
					//number = Integer.toString(dbresult.getInt("number")); //auskommentiert, da hier die führende 0 entfernt wird
					number = dbresult.getString("number");
				}
    		}else{
    			//Test nicht bestanden
    			Logger.writeLog("LagerplatzOutOfBounceException geworfen");
    			LagerplatzOutOfBounceException loobe = new LagerplatzOutOfBounceException();
    			Logger.writeLog(loobe);
    			throw loobe;
    		}
   		}catch(SQLTimeoutException tout){
   			Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
			Logger.writeLog(new DatenbankUnerreichbarException(tout.getMessage()));
			throw new DatenbankUnerreichbarException(tout.getMessage());
		}catch (SQLException sqle) {
			Logger.writeLog("SQLException gefangen und weitergeworfen");
			Logger.writeLog(sqle);
			throw sqle;
		}
    	Logger.writeLog("Rueckgabe: " + number);
		return number;
    }

    @Override
    public String getPlaceID(String pid) throws SQLException, DatenbankUnerreichbarException /*, ProductNotFoundException*/ {
    	Logger.writeLog("Backend: getPlaceID aufgerufen");
    	Logger.writeLog("Parameter pid: " + pid);
    	ResultSet dbresult = null;
    	String placeid = "";
		
    	
		try {
			dbresult = statement.executeQuery("SELECT storage_place FROM parts WHERE number = '" + pid + "'");
			while (dbresult.next()){
				placeid = Integer.toString(dbresult.getInt("storage_place"));
			}
			
			if(placeid.equals("")){
				/*Logger.writeLog("ProductNotFoundException geworfen");
				ProductNotFoundException pnfe = new ProductNotFoundException();
    			Logger.writeLog(pnfe);
    			throw pnfe;*/
			}
			
		}catch(SQLTimeoutException tout){
			Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
			Logger.writeLog(new DatenbankUnerreichbarException(tout.getMessage()));
			throw new DatenbankUnerreichbarException(tout.getMessage());
		} catch (SQLException sqle) {
			Logger.writeLog("SQLException gefangen und weitergeworfen");
			Logger.writeLog(sqle);
			throw sqle;
		}
		Logger.writeLog("Rueckgabe: " + placeid);
		return placeid;
    }

    @Override
    public void setNewProduct(String pid, String place) throws SQLException, DatenbankUnerreichbarException, LagerplatzBelegtException, LagerplatzOutOfBounceException{
    	Logger.writeLog("Backend: setNewProduct aufgerufen");
    	Logger.writeLog("Parameter pid: " + pid);
    	Logger.writeLog("Parameter place: " + place);
    	
    	/**
    	 * LagerplatzOutOfBounce:
    	 * Tritt auf, wenn Parameter place über der Lagerplatzgrenze liegt.
    	 */
    	try{
    		//Test auf LagerPlatzOutOfBounce
    		testLagerplatzExistiert(place);
    		
    		Logger.writeLog("Lagerplatzstelle gibt es");
    		//Test auf Lagerplatzbelegt
    		statement.executeUpdate("");
		
    			//Exceptionfälle:
    			//LagerplatzOutOfBounce, wenn place > 100 oder < 0 Implementiert
    			//LagerplatzBelegt, wenn
    			//		- PID schon vorhanden -> Test ob es ein Produkt gibt, für das number = pid gilt
    			//		- Lagerplatz schon vorhanden -> Test ob es ein Produkt gibt, für das storage_place = (Select id from storage where place = " + place) gilt
			statement.executeUpdate("INSERT INTO parts (number, storage_place) VALUES ('" + pid + "', (SELECT id from storage WHERE storage.place = " + place + "))");
			
			Logger.writeLog("Packet gespeichert. ID: " + pid + " Platz: " + place);
			
    	}catch(LagerplatzOutOfBounceException loobe){
    		Logger.writeLog("LagerplatzOutOfBounceException geworfen");
			Logger.writeLog(loobe);
			throw loobe;
		}catch(SQLTimeoutException tout){
			Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
			Logger.writeLog(new DatenbankUnerreichbarException(tout.getMessage()));
			throw new DatenbankUnerreichbarException(tout.getMessage());
		}catch(SQLException sqle){
			
			if (sqle.getErrorCode() == uniqueconstraintfailed){
				Logger.writeLog("SQLException gefangen und als LagerplatzBelegtException weitergeworfen");
				Logger.writeLog(new LagerplatzBelegtException(sqle.getMessage()));
				throw new LagerplatzBelegtException(sqle.getMessage());
			}else{
				Logger.writeLog("SQLException gefangen und weitergeworfen");
				Logger.writeLog(sqle);
				throw sqle;
			}
		}
	}
       

    @Override
	public void setDelProduct(String pid) throws SQLException, DatenbankUnerreichbarException {
    	Logger.writeLog("Backend: setDelProduct aufgerufen");
    	Logger.writeLog("Parameter pid: " + pid);
    	
   		try {
			statement.executeUpdate("DELETE FROM parts WHERE number = '" + pid + "'");
   		}catch(SQLTimeoutException tout){
   			Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
   			DatenbankUnerreichbarException dbue = new DatenbankUnerreichbarException(tout.getMessage());
			Logger.writeLog(dbue);
			throw dbue;
		} catch (SQLException sqle) {
			Logger.writeLog("SQLException gefangen und weitergeworfen");
			Logger.writeLog(sqle);
			throw sqle;
		}
	}
    

    @Override
    public boolean testUnusedPID(String pid) throws DatenbankUnerreichbarException, SQLException {
    	Logger.writeLog("Backend: testUnusedPID aufgerufen");
    	Logger.writeLog("Parameter pid: " + pid);
    	ResultSet dbresult = null;
    	boolean status = false;
		try {
			dbresult = statement.executeQuery("SELECT CASE WHEN EXISTS (SELECT * FROM [parts] WHERE number = '" + pid + "') THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS name");
			status = Boolean.parseBoolean(dbresult.getString("name"));
			Logger.writeLog("Status: " + status);
		}catch(SQLTimeoutException tout){
			Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
			Logger.writeLog(new DatenbankUnerreichbarException(tout.getMessage()));
			throw new DatenbankUnerreichbarException(tout.getMessage());
		} catch (SQLException sqle) {
			Logger.writeLog("SQLException gefangen und weitergeworfen");
			Logger.writeLog(sqle);
			throw sqle;
		}
		Logger.writeLog("Rueckgabe: " + status);
		return status;
    }

    @Override
    public void setUpdateNumber(String value, String id) throws SQLException, DatenbankUnerreichbarException /* throws UpdateErrorException */ {
    	Logger.writeLog("Backend: setUpdateNumber aufgerufen");
    	Logger.writeLog("Paramter value" + value);
    	Logger.writeLog("Paramter id" + id);
    	try {
    		statement.executeUpdate(String.format("UPDATE parts SET number=%s WHERE number=%s", value, id ));	
    	}catch(SQLTimeoutException tout){
    		Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
			Logger.writeLog(new DatenbankUnerreichbarException(tout.getMessage()));
			throw new DatenbankUnerreichbarException(tout.getMessage());
		} catch (SQLException sqle) {
			Logger.writeLog("SQLException gefangen und weitergeworfen");
			Logger.writeLog(sqle);
			throw sqle;
		}
	}
    
    @Override
    public void setUpdatePlace(String value, String pid) throws DatenbankUnerreichbarException, SQLException/* throws UpdateErrorException */ {
    	Logger.writeLog("Backend: setUpdatePlace aufgerufen");
    	Logger.writeLog("Paramter value" + value);
    	Logger.writeLog("Paramter pid" + pid);
    	try {
    		statement.executeUpdate(String.format("UPDATE parts SET storage_place='%s' WHERE number='%s'", value, pid ));	
    	}catch(SQLTimeoutException tout){
    		Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
			Logger.writeLog(new DatenbankUnerreichbarException(tout.getMessage()));
			throw new DatenbankUnerreichbarException(tout.getMessage());
		} catch (SQLException sqle) {
			Logger.writeLog("SQLException gefangen und weitergeworfen");
			Logger.writeLog(sqle);
			throw sqle;
		}
	}
    
    /**
     * @author christian
     * 
     * erstmal Quick and Dirty, aber die Datenbankstruktur gibt nicht mehr her.
     * 
     * Es wird ein freier Platz gesucht, und das Produkt temp. hingebucht.
     * Wenn es keinen freien Platz gibt, geht diese FUnktion nicht.
     * @throws LagerplatzOutOfBounceException 
     * @throws DatenbankUnerreichbarException 
     * @throws SQLException 
     */

	@Override
	public void setSwapPlaces(String placeid_1, String placeid_2) throws SQLException, DatenbankUnerreichbarException, LagerplatzOutOfBounceException{
		Logger.writeLog("Backend: setSwap aufgerufen");
		Logger.writeLog("Parameter placeid_1: " + placeid_1);
		Logger.writeLog("Parameter placeid_2: " + placeid_2);
		String pid_1 = getPID(placeid_1);
		String pid_2 = getPID(placeid_2);
		
		Logger.writeLog("ermittelter pid_1: " + pid_1);
		Logger.writeLog("ermittelter pid_2: " + pid_2);
		
		String free_place = getAllUnusedPlaces().getLast();
		

		if (pid_2 != "" && pid_1 != ""){
			Logger.writeLog("Swappen von 2 vollen Lagerplaetzen");
			// Auf temp. Platz buchen
			setUpdatePlace(free_place, pid_2);
			
			//set swap
			setUpdatePlace(placeid_2, pid_1);
			setUpdatePlace(placeid_1, pid_2);
		} else if (pid_1 != "" && pid_2 == "") {
			Logger.writeLog("Swappen von 1. vollen Platz und 2. leeren Platz");
			//Lagerplatz 2 ist frei
			setUpdatePlace(placeid_2, pid_1);
		} else if (pid_1 == "" && pid_2 != ""){
			Logger.writeLog("Swappen von 1. leeren Platz und 2. vollen Platz");
			//Lagerplatz 1 ist frei
			setUpdatePlace(placeid_1, pid_2);
		}
	}
	
	
	
	/**
	 * Diese Methode prüft die Einhaltung der Lagergrenze.
	 * @param place Der Lagerplatz 
	 * @return true Wenn place als Lagerplaz existiert
	 * @throws LagerplatzOutOfBounceException wenn place als Lagerplatz nicht existiert
	 */
	private boolean testLagerplatzExistiert(String place) throws LagerplatzOutOfBounceException, DatenbankUnerreichbarException, SQLException{
		Logger.writeLog("Backend: testLagerplatzExistiert aufgerufen");
		Logger.writeLog("Parameter place: " + place);
		boolean bool = false;
		int maxLagerplatz = 0;
		int minLagerplatz = Integer.MAX_VALUE;
		int place2 = Integer.parseInt(place);
	
		try{
			ResultSet dbresultMax = statement.executeQuery("SELECT max(place) from storage");
			while (dbresultMax.next()){
				maxLagerplatz = dbresultMax.getInt("max(place)");
			}
			
			ResultSet dbresultMin = statement.executeQuery("SELECT min(place) from storage");
			while (dbresultMin.next()){
				minLagerplatz = dbresultMin.getInt("min(place)");
			}
			
			if ((minLagerplatz <= place2) && (place2 <= maxLagerplatz)){
				Logger.writeLog("Lagerplatz gibt es");
				bool = true;
			}else{
				Logger.writeLog("LagerplatzOutOfBounceException geworfen");
    			LagerplatzOutOfBounceException loobe = new LagerplatzOutOfBounceException();
    			Logger.writeLog(loobe);
    			throw loobe;
			}
			Logger.writeLog("Rueckgabe: " + bool);
			return bool;
			
		}catch(SQLTimeoutException tout){
			Logger.writeLog("SQLTimeoutException gefangen und als DatenbankUnerreichbarException weitergeworfen");
			Logger.writeLog(new DatenbankUnerreichbarException(tout.getMessage()));
			throw new DatenbankUnerreichbarException(tout.getMessage());
		}catch (SQLException sqle) {
			Logger.writeLog("SQLException gefangen und weitergeworfen");
			Logger.writeLog(sqle);
			throw sqle;
		}
	}
	
	
	//Dokumentation
	
	//Middleware-Fails:
	// - Exceptions werden an Konsole ausgegeben
	
	//FrontEnd-Fails:
	// - Produkt Verändrn: Es befindet sich genau ein Paket im Lager. Dieses sortiert man an einen
	//anderen Lagerplatz. Die Anzeige "Quell Stellplatz" bleibt unverändert obwohl das Paket dort
	//nicht mehr ist.
	// - Produkt hinzufügen: Fügt man zwei mal das gleiche Produkt hinzu, wird der Benutzer nicht
	//darüber benachrichtigt, dass das nicht geht.
	
	//testUnusedPlaceID: irgendein Boolean-Fehler? => Wird durch GetPID ersetzt.
	// => Neue Methode GetFreePlaces(), Name und Interface noch abstimmen.
	
	//GetID => SELECT place, number  FROM storage LEFT JOIN parts ON (storage.place = parts.storage_place);
	
}


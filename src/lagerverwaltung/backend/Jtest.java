package lagerverwaltung.backend;



import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.LinkedList;

import lagerverwaltung.exceptions.DatenbankUnerreichbarException;
import lagerverwaltung.exceptions.LagerVollException;
import lagerverwaltung.exceptions.LagerplatzBelegtException;
import lagerverwaltung.exceptions.LagerplatzOutOfBounceException;

import org.junit.FixMethodOrder;

import org.junit.Test;

import org.junit.runners.MethodSorters;

/**
 * @author: Christian Bäetz
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Jtest {
	
	DBWrapper dbconn = null;
	int storageSlots = 100;		//erwartete anzahl lagerpl�tze
	
	public Jtest(){
		try {
			try {
				dbconn = new DBWrapper();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dbconn.initialiseDB();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (DatenbankUnerreichbarException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void clearDatabase() throws SQLException, DatenbankUnerreichbarException{
		LinkedList<String> element = dbconn.getAllPIDs();
		for (String s : element) {
			dbconn.setDelProduct(s);
		}
		
	}
	
	/**
	 * @author: Christian Bäetz
	 */
	
	@Test
	public void testAnzahlLagerSlots_initialise(){

		try {
			try {
				dbconn.initialiseDB();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (DatenbankUnerreichbarException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*
		 * Zahl der Lagerpl�tze werden verglichen
		 */
		LinkedList<String> slots = null;
		
		try {
			slots = dbconn.getAllPlaces();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatenbankUnerreichbarException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertEquals(storageSlots, slots.size());
		
	}
	
	/**
	 * @author: Christian Bäetz
	 */
	
	@Test
	public void testAnzahlPIDs() throws SQLException, DatenbankUnerreichbarException{
		clearDatabase();

		try {
			assertEquals(dbconn.getAllPIDs().size(), 0);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (DatenbankUnerreichbarException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			try {
				dbconn.setNewProduct("505", "32");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DatenbankUnerreichbarException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dbconn.setNewProduct("506", "36");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DatenbankUnerreichbarException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dbconn.setNewProduct("507", "47");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DatenbankUnerreichbarException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dbconn.setNewProduct("508", "2");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DatenbankUnerreichbarException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				dbconn.setNewProduct("509", "100");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DatenbankUnerreichbarException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (LagerplatzBelegtException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LagerplatzOutOfBounceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			assertEquals(dbconn.getAllPIDs().size(), 5);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatenbankUnerreichbarException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * @author: Christian Bäetz
	 */
	
	@Test
	public void testSingleProductInsert() throws SQLException, LagerplatzBelegtException, LagerplatzOutOfBounceException, DatenbankUnerreichbarException{
		/*
		 * 1. DB wid neu angelegt
		 * 2. pr�fen das Produkanzahl 0 ist
		 * 3. Es wird ein Produkt eingef�gt
		 * 4. pr�fen das Produkanzahl 1 ist
		 * 5. pr�fen das produkt-number und lageris stimmten
		 */
		clearDatabase();
		try {
			assertEquals(dbconn.getAllPIDs().size(), 0);
		} catch (DatenbankUnerreichbarException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			dbconn.setNewProduct("505", "32");
		} catch (DatenbankUnerreichbarException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			assertEquals(dbconn.getAllPIDs().size(), 1);
		} catch (DatenbankUnerreichbarException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			try {
				assertEquals(dbconn.getPID("32"), "505");
			} catch (DatenbankUnerreichbarException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (LagerplatzOutOfBounceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * @author: Christian Bäetz
	 */
	
	@Test
	public void testGetPlaceID_noError() throws LagerplatzBelegtException, LagerplatzOutOfBounceException, DatenbankUnerreichbarException, SQLException{

		clearDatabase();
		
		
		try {
			dbconn.setNewProduct("505", "32");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			dbconn.setNewProduct("50", "3");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			dbconn.setNewProduct("5", "100");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			assertEquals(dbconn.getPlaceID("505"), "32");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			assertEquals(dbconn.getPlaceID("50"), "3");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			assertEquals(dbconn.getPlaceID("5"), "100");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * @author Soldan
	 * @throws LagerplatzOutOfBounceException 
	 * @throws LagerplatzBelegtException 
	 */
	@Test
	public void testNextBestPlaces() throws SQLException, DatenbankUnerreichbarException, LagerVollException, LagerplatzBelegtException, LagerplatzOutOfBounceException{
	    clearDatabase();
	    LinkedList<String> test = new LinkedList<String>();
	    test.add("1");
	    assertEquals(dbconn.getNextBestPlace(), test);
	    dbconn.setNewProduct("404", "1");
	    test.clear();
	    test.add("2");
	    assertEquals(dbconn.getNextBestPlace(), test);
	}
	/**
	 * @author Soldan
	 * @throws LagerplatzOutOfBounceException 
	 * @throws LagerplatzBelegtException 
	 */
	@Test
	public void testGetAllUsedPlaces() throws SQLException, DatenbankUnerreichbarException, LagerVollException, LagerplatzBelegtException, LagerplatzOutOfBounceException{
	    clearDatabase();
	    LinkedList<String> list = new LinkedList<String>();
	    assertEquals(dbconn.getAllUsedPlaces(), list);
	    dbconn.setNewProduct("404", "1");
	    list.add("1");
	    assertEquals(dbconn.getAllUsedPlaces(), list);
	    dbconn.setNewProduct("405", "2");
	    dbconn.setNewProduct("402", "5");
	    list.add("2");
	    list.add("5");
	    assertEquals(dbconn.getAllUsedPlaces(), list);
	}
	/**
	 * @author Soldan
	 * @throws SQLException 
	 * @throws DatenbankUnerreichbarException 
	 * @throws LagerplatzOutOfBounceException 
	 * @throws LagerplatzBelegtException 
	 */
	/*@Test
	public void testSetUpdatePlaces() throws DatenbankUnerreichbarException, SQLException, LagerplatzBelegtException, LagerplatzOutOfBounceException{
	    clearDatabase();
	    dbconn.setNewProduct("405", "3");
	    dbconn.setUpdatePlace("4", "405");
	    assertEquals(dbconn.getPlaceID("405"), "4");
	}*/
	
	/**
	 * @author Soldan
	 * @throws DatenbankUnerreichbarException 
	 * @throws SQLException 
	 * @throws LagerplatzOutOfBounceException 
	 * @throws LagerplatzBelegtException 
	 */
	@Test
	public void testSetDelProduct() throws SQLException, DatenbankUnerreichbarException, LagerplatzBelegtException, LagerplatzOutOfBounceException{
	    clearDatabase();
	    LinkedList<String> list= new LinkedList<String>();
	    dbconn.setNewProduct("504", "3");
	    assertEquals(dbconn.getPlaceID("504"), "3");
	    dbconn.setDelProduct("504");
	    assertEquals(dbconn.getAllUsedPlaces().size(), 0);
	    
	    dbconn.setDelProduct("50455");
	    
	}

	
	@Test
	public void testSetUpdatePlace() throws SQLException, DatenbankUnerreichbarException, LagerplatzBelegtException, LagerplatzOutOfBounceException{
		clearDatabase();
		dbconn.setNewProduct("505", "32");
		
		assertEquals(dbconn.getPlaceID("505"), "32");
		
		dbconn.setUpdatePlace("75", "505");
		
		assertEquals(dbconn.getPlaceID("505"), "75");
		
	}
	
	
	
	@Test
	public void testSwapPlaces() throws SQLException, DatenbankUnerreichbarException, LagerplatzBelegtException, LagerplatzOutOfBounceException{
		//init
		
		clearDatabase();
		
		dbconn.setNewProduct("505", "32");
		dbconn.setNewProduct("50", "3");
		
		//swap zwei belegt plätze
		
		dbconn.setSwapPlaces("32", "3");
		
		assertEquals(dbconn.getPlaceID("505"), "3");
		assertEquals(dbconn.getPlaceID("50"), "32");
		
		
		//swap einen platz
		dbconn.setSwapPlaces("32", "50");
		
		assertEquals(dbconn.getPlaceID("50"), "50");
		//aktueller Platz = 50
		
		//swap einen platz, letzter Platz
		dbconn.setNewProduct("1234", "99");
		dbconn.setSwapPlaces("2", "99");
		
		assertEquals(dbconn.getPlaceID("1234"), "2");
		
		
		
		
	}
	


}

package lagerverwaltung.backend;

import java.sql.SQLException;
import java.util.LinkedList;
import lagerverwaltung.exceptions.*;

/**
 * Dies ist das Interface um mit der Datenbank kommunizieren zu koennen.
 * Der Methodeninhalt selbst wird vom dem Backend bereitgestellt.
 * @author Marco
 */
public interface DBConnect {


	/**
	 * Die Methode fuer das Herstellen der Verbindung. Sie darf nur einmal ausgefuehrt werden!
	 * Ansonsten wird die gesamte DB geloescht! Vielleicht sollte man hier noch eine Art Schranke hinzufuegen ^^
	 * Auch muss sich noch auf den Rueckgabetyp / Exception geeinigt werden.
	 * @return boolean True: DB-Connection erfolgreich False: DB-Connection fehlgeschlagen
	 * @throws DatenbankUnerreichbarException wenn beim Verbindungsaufbau etwas schief geht
	 * @throws SQLException bei anderweitigen Exceptions mit der DB
	 */
	boolean initialiseDB() throws SQLException, DatenbankUnerreichbarException;
	
	/**
	 * Diese Methode gibt den naechstbesten freien Lagerplatz zurueck
	 * @return LinkedList<String> die einen Lagerplatz beinhaltet
	 * @throws LagerVollException wenn kein weiteres Packet mehr in das Lager passt
	 * @throws SQLException bei anderweitigen Exceptions mit der DB
	 */
	LinkedList<String>getNextBestPlace() throws SQLException, DatenbankUnerreichbarException, LagerVollException;
	
	/**
	 * Diese Methode gibt alle freien Lagerplaetze zurueck
	 * @return LinkedList<String> die alle freien Lagerplaetze enthaelt
	 * @throws LagerVollException wenn kein weiteres Packet mehr in das Lager passt
	 * @throws DatenbankUnerreichbarException wenn keien Verbindung zur DB besteht
	 * @throws SQLException bei anderweitigen Exceptions mit der DB
	 */
	LinkedList<String>getAllPlaces() throws SQLException, DatenbankUnerreichbarException;
	
	/**
	 * Diese Methode gibt alle verwendeten Produkt-IDs zurueck
	 * @return LinkedList<String>, die alle verwendeten Produkt-IDs beinhaltet. 
	 * Wird keine P-ID verwendet, so ist die Liste leer.
	 * @throws DatenbankUnerreichbarException wenn keien Verbindung zur DB besteht
	 * @throws SQLException bei anderweitigen Exceptions mit der DB
	 */
	LinkedList<String>getAllPIDs() throws SQLException, DatenbankUnerreichbarException;
	
	/**
	 * Diese Methode erwartet einen Lagerplatz und gibt die PID des dortigen Produkts zurueck
	 * @param place Ist der Lagerplatz an dem nach der PID geschaut wird
	 * @return String der eine PID darstellt. Ist kein Produkt auf dem Lagerplatz, dann ist der
	 * String leer.
	 * @throws LagerplatzOutOfBounceException wenn ein Lagerplatz angesprochen wird, den es so nicht gibt
	 * @throws DatenbankUnerreichbarException wenn keien Verbindung zur DB besteht
	 * @throws SQLException bei anderweitigen Exceptions mit der DB
	 */
	String getPID(String place) throws SQLException, DatenbankUnerreichbarException, LagerplatzOutOfBounceException;
	
	/**
	 * Diese Methode erwartet eine Produkt-ID und gibt den Lagerplatz des Produtes zurueck
	 * @param pid Eine Produkt-ID
	 * @return String der einen Lagerplatz darstellt. Gibt es die PID nicht, dann wird ein leerer
	 * String zuruekgegeben.
	 * @throws DatenbankUnerreichbarException wenn keien Verbindung zur DB besteht
	 * @throws SQLException bei anderweitigen Exceptions mit der DB
	 */
	String getPlaceID(String pid) throws SQLException, DatenbankUnerreichbarException, ProductNotFoundException;
	
	/**
	 * Diese Methode speichert ein Produkt in dem Lager.
	 * @param pid Eine Produkt-ID
	 * @param place Einen Lagerplatz
	 * @throws LagerplatzOutOfBounceException wenn ein Lagerplatz angesprochen wird, den es so nicht gibt
	 * @throws LagerplatzBelegtException wenn auf dem Lagerplatz schon ein Packet ist und dieses ueberschrieben werden soll
	 * @throws DatenbankUnerreichbarException wenn keien Verbindung zur DB besteht
	 * @throws SQLException bei anderweitigen Exceptions mit der DB
	 */
	void setNewProduct(String pid, String place) throws SQLException, DatenbankUnerreichbarException, LagerplatzBelegtException, LagerplatzOutOfBounceException;

	
	/**
	 * Diese Methode loescht ein Produkt aus dem Lager.
	 * @param pid Eine Produkt-ID
	 * @throws DatenbankUnerreichbarException wenn keien Verbindung zur DB besteht
	 * @throws SQLException bei anderweitigen Exceptions mit der DB
	 */
	void setDelProduct(String pid) throws SQLException, DatenbankUnerreichbarException;
	
	/**
	 * Diese Methode erwartet eine Produkt-ID und schaut ob sich diese schon im Lager befindet.
	 * @param pid Eine Produkt-ID
	 * @return boolean True: Produkt-ID unbenutzt -- False: Produkt-ID schon benutzt
	 * @throws DatenbankUnerreichbarException wenn keien Verbindung zur DB besteht
	 * @throws SQLException bei anderweitigen Exceptions mit der DB
	 */
	@Deprecated
	boolean testUnusedPID(String pid) throws DatenbankUnerreichbarException, SQLException;
	
	/**
	 * Diese Methode gibt alle belegten Lagerplätze zurück.
	 * @throws DatenbankUnerreichbarException wenn keien Verbindung zur DB besteht
	 * @throws SQLException bei anderweitigen Exceptions mit der DB
	 */
	LinkedList<String> getAllUsedPlaces() throws SQLException, DatenbankUnerreichbarException;
	
	/**
	 * Diese Methode erwartet zwei Lagerplätze und tauscht die Packete im Lager.
	 * @throws DatenbankUnerreichbarException wenn keien Verbindung zur DB besteht
	 * @throws SQLException bei anderweitigen Exceptions mit der DB
	 * @throws LagerplatzOutOfBounceException wenn ein Lagerplatz angesprochen wird, den es so nicht gibt
	 */
	void setSwapPlaces(String placeid_1, String placeid_2) throws SQLException, DatenbankUnerreichbarException, LagerplatzOutOfBounceException;

	
	/**
	 * Methode speichert ein Produkt auf einen neuen Lagerplatz
	 * 
	 * @param value Angabe des neuen Lagerplatzes
	 * @param id	pid, auf welcher der Änderung
	 * @throws DatenbankUnerreichbarException wenn keien Verbindung zur DB besteht
	 * @throws SSQLException bei anderweitigen Exceptions mit der DB
	 */
	void setUpdatePlace(String value, String id)
			throws DatenbankUnerreichbarException, SQLException;

	/**
	 * Methode dienst dazu ein Produkt um zu benennen.
	 * 
	 * @param value Angabe der neuen pid
	 * @param id	Alte pid
	 * @throws SQLException
	 * @throws DatenbankUnerreichbarException
	 */
	void setUpdateNumber(String value, String id) throws SQLException,
			DatenbankUnerreichbarException;
	
}//Klasse

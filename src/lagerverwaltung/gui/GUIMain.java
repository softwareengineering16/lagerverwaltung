package lagerverwaltung.gui;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import lagerverwaltung.backend.*;
import lagerverwaltung.middleware.*;

public class GUIMain extends Application {
	private Storage storage;
	private DBConnect database;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			database 	= new DBWrapper();
			storage 	= new Storage(database);
			
			ControllerMain controller 	= new ControllerMain(storage);
			FXMLLoader loader 			= new FXMLLoader(getClass().getResource("fxml/Mainwindow.fxml"));
			loader.setController(controller);
			TabPane root 					= (TabPane)loader.load();
			Scene scene 				= new Scene(root,root.getPrefWidth(), root.getPrefHeight());
			
			scene.getStylesheets().add(getClass().getResource("fxml/application.css").toExternalForm());

			primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("ressource/icon.png")));
			primaryStage.setTitle("GSE Lagerverwaltungssoftware");
			primaryStage.setScene(scene);
			primaryStage.sizeToScene();
			primaryStage.show();
		
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
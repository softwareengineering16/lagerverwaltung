package lagerverwaltung.gui;

import java.net.URL;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Window;
import javafx.util.Callback;
import javafx.util.Duration;
import lagerverwaltung.exceptions.DatenbankUnerreichbarException;
import lagerverwaltung.exceptions.LagerVollException;
import lagerverwaltung.exceptions.LagerplatzBelegtException;
import lagerverwaltung.exceptions.LagerplatzOutOfBounceException;
import lagerverwaltung.exceptions.PaketSchonVorhandenException;
import lagerverwaltung.middleware.Space;
import lagerverwaltung.middleware.Storage;

public class ControllerMain {
	
	private Storage storage;
	private ObservableList<Space> allSpaces;
	private ObservableList<Space> usedSpaces;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button searchButtonPlace;

    @FXML
    private Button searchButtonPlace2;
    
    @FXML
    private Button searchButtonPlaceSrc;
    
    @FXML
    private Button searchButtonPlaceDest;

    @FXML
    private Button searchButtonId;
    
    @FXML
    private Button searchButtonId2;

    @FXML
    private Button searchButtonIdSrc;
    
    @FXML
    private Button searchButtonIdDest;
    
    @FXML
    private TextField search1;

    @FXML
    private TextField search2;

    @FXML
    private TextField search3;
    
    @FXML
    private ComboBox<Space> productsDelete;

    @FXML
    private ComboBox<Space> destSpace;
    
    @FXML
    private ComboBox<Space> srcSpace;
    
    @FXML
    private TextField productIdAdd;

    @FXML
    private Button buttonApplyAdd;

    @FXML
    private Button buttonDelete;

    @FXML
    private Button confirmButtonChange;
    
    @FXML
    private ListView<Space> listProducts;
    
    @FXML
    private Label addMessageLabel;
    
    @FXML
    private TabPane tabPane;

    @FXML
    void add(ActionEvent event){
    	String id;
    	switch(productIdAdd.getText().length()){
    	case 1: id = "000" + productIdAdd.getText();
    	break;
    	case 2: id = "00" + productIdAdd.getText();
    	break;
    	case 3: id = "0" + productIdAdd.getText();
    	break;
    	default: id = productIdAdd.getText();
     	}
    	
    	
    	if(!productIdAdd.getText().isEmpty()){
			try {
				labelInformation();
				lagerverwaltung.middleware.Package pack = storage.newPackage(id);
				storage.addPackage(pack);
				refreshUsedSpaces();
			} catch (LagerVollException | DatenbankUnerreichbarException e) {		
			} catch (SQLException e) {		
			} catch (LagerplatzBelegtException e) {
				addMessageLabel.setVisible(false);
				packageAlreadyAlert();
			} catch (LagerplatzOutOfBounceException e) {
			} catch (PaketSchonVorhandenException e) {			
			}
    	}
    }

    @FXML
    void delete(ActionEvent event) throws SQLException, DatenbankUnerreichbarException {
    	deleteAlert();
    }

    @FXML
    void confirmChange(ActionEvent event) {
    	changeAlert();
    }
    
    public ControllerMain(Storage storage){   
    	this.storage = storage;
    }

    @FXML
    void initialize() {
        assert searchButtonPlace 	!= null : "fx:id=\"searchButtonPlace\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert searchButtonPlace2 	!= null : "fx:id=\"searchButtonPlace1\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert searchButtonPlaceSrc != null : "fx:id=\"searchButtonPlaceSrc\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert searchButtonPlaceDest!= null : "fx:id=\"searchButtonPlaceDest\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert searchButtonId 		!= null : "fx:id=\"searchButtonId\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert searchButtonId2 		!= null : "fx:id=\"searchButtonId1\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert searchButtonIdSrc	!= null : "fx:id=\"searchButtonIdSrc\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert searchButtonIdDest	!= null : "fx:id=\"searchButtonIdDest\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert search1 				!= null : "fx:id=\"search1\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert search2 				!= null : "fx:id=\"search2\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert search3 				!= null : "fx:id=\"search3\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert srcSpace 			!= null : "fx:id=\"srcSpace\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert productsDelete 		!= null : "fx:id=\"productsDelete\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert productIdAdd 		!= null : "fx:id=\"productIdAdd\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert buttonApplyAdd 		!= null : "fx:id=\"buttonApplyAdd\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert destSpace 			!= null : "fx:id=\"destSpace\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert buttonDelete 		!= null : "fx:id=\"buttonDelete\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert confirmButtonChange 	!= null : "fx:id=\"confirmButtonChange\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert listProducts 		!= null : "fx:id=\"listProducts\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        assert addMessageLabel 		!= null : "fx:id=\"addMessageLabel\" was not injected: check your FXML file 'Mainwindow.fxml'.";
        
        formatTextField(productIdAdd);
        formatTextField(search1);
        formatTextField(search3);
        formatTextField(search2);
        setupKeyControlls();
        
        allSpaces = storage.getSpaces();
        usedSpaces = storage.getUsedSpaces();
        
        listProducts.setItems(allSpaces);
        productsDelete.setItems(usedSpaces);	// delete comboBox has to contain just the used spaces
        if(!usedSpaces.isEmpty()){
        productsDelete.setValue(usedSpaces.get(0));
        }
        srcSpace.setItems(usedSpaces);	// source comboBox has to contain just the used spaces
        destSpace.setItems(allSpaces);	// destination comoBox has to contain all possible spaces 
        if(!usedSpaces.isEmpty()){
        	srcSpace.setValue(usedSpaces.get(0));
        }
        destSpace.setValue(allSpaces.get(0));
    }

    private void setupKeyControlls(){
    	EventHandler<KeyEvent> handler = new EventHandler<KeyEvent>(){

			@Override
			public void handle(KeyEvent e) {
				if(e.getCode() == KeyCode.F2){tabPane.getSelectionModel().select(0);}
				if(e.getCode() == KeyCode.F3){tabPane.getSelectionModel().select(1);}
				if(e.getCode() == KeyCode.F4){tabPane.getSelectionModel().select(2);}
				if(e.getCode() == KeyCode.F5){tabPane.getSelectionModel().select(3);}
				
			}
    	};
    	EventHandler<KeyEvent> enterHandler = new EventHandler<KeyEvent>(){

			@Override
			public void handle(KeyEvent e) {
				if(e.getCode() == KeyCode.ENTER){buttonApplyAdd.fire();}
				if(e.getCode() == KeyCode.F2){tabPane.getSelectionModel().select(0);}
				if(e.getCode() == KeyCode.F3){tabPane.getSelectionModel().select(1);}
				if(e.getCode() == KeyCode.F4){tabPane.getSelectionModel().select(2);}
				if(e.getCode() == KeyCode.F5){tabPane.getSelectionModel().select(3);}
			}
    	};
    	
    	tabPane.setOnKeyPressed(handler);
    	productIdAdd.setOnKeyPressed(enterHandler);
    	search1.setOnKeyPressed(handler);
    	search2.setOnKeyPressed(handler);
    	search3.setOnKeyPressed(handler);
    }
    
    private void labelInformation() throws LagerVollException, SQLException, DatenbankUnerreichbarException{
    	String id;
    	switch(productIdAdd.getText().length()){
    	case 1: id = "000" + productIdAdd.getText();
    	break;
    	case 2: id = "00" + productIdAdd.getText();
    	break;
    	case 3: id = "0" + productIdAdd.getText();
    	break;
    	default: id = productIdAdd.getText();
     	}
    	
    	addMessageLabel.setText("Paket " + id + " an Position " + storage.findNextFreeSpace().getNumber() + " eingelagert!");
    	addMessageLabel.setVisible(true);
    	
    	Timeline timeline = new Timeline(new KeyFrame(
    	        Duration.millis(6000),
    	        ae -> addMessageLabel.setVisible(false)));
    	timeline.play();
    }
    
    private void changeAlert(){
    	Alert cancelAlert = new Alert(AlertType.CONFIRMATION);
    	cancelAlert.setTitle("Best�tigung");
    	cancelAlert.setHeaderText("Wollen sie die Eintr�ge wirklich umsortieren?");
    	
    	Optional<ButtonType> result = cancelAlert.showAndWait();
    	if (result.get() == ButtonType.OK){
    	    storage.changePackage(srcSpace.getValue(), destSpace.getValue());
    	    refreshUsedSpaces();
    	} else {
    	    cancelAlert.close();
    	}
    }
    
    private void deleteAlert() throws SQLException, DatenbankUnerreichbarException{
    	Alert cancelAlert = new Alert(AlertType.CONFIRMATION);
    	cancelAlert.setTitle("Best�tigung");
    	cancelAlert.setHeaderText("Wollen sie den Eintrag wirklich l�schen?");
    	
    	Optional<ButtonType> result = cancelAlert.showAndWait();
    	if (result.get() == ButtonType.OK){
    	    storage.destroyPackage(productsDelete.getValue().getPacket());
    	    deleteRefresh();
    	    usedSpaces = storage.getUsedSpaces();
    	    if(searchButtonId2.getText().equals("Zur�ck")){
    			refreshUsedSpaces();
    		}
    	} else {
    	    cancelAlert.close();
    	}
    }
    
    private void deleteRefresh(){
    	ObservableList<Space> usedSpaces = storage.getUsedSpaces();
    	productsDelete.setItems(usedSpaces);
    	if(!usedSpaces.isEmpty()){
    		productsDelete.setValue(usedSpaces.get(0));
    	}
    	refreshUsedSpaces();
    }
    
    private void storageFullAlert(){
    	Alert cancelAlert = new Alert(AlertType.ERROR);
    	cancelAlert.setTitle("Fehler");
    	cancelAlert.setHeaderText("Das Lager ist voll!");
    	
    	Optional<ButtonType> result = cancelAlert.showAndWait();
    	if (result.get() == ButtonType.OK){
    		cancelAlert.close();
    	}
    }
    
    private void packageAlreadyAlert(){
    	Alert cancelAlert = new Alert(AlertType.ERROR);
    	cancelAlert.setTitle("Fehler");
    	cancelAlert.setHeaderText("Es ist bereits ein Packet mit dieser ID vorhanden!");
    	
    	Optional<ButtonType> result = cancelAlert.showAndWait();
    	if (result.get() == ButtonType.OK){
    		cancelAlert.close();
    	}
    }
    
    private void refreshUsedSpaces(){
    	usedSpaces = storage.getUsedSpaces();
		productsDelete.setItems(usedSpaces);
		if(!usedSpaces.isEmpty()){
			productsDelete.setValue(usedSpaces.get(0));
		}
		destSpace.setItems(allSpaces);
		destSpace.setValue(allSpaces.get(0));
		srcSpace.setItems(usedSpaces);
		if(!usedSpaces.isEmpty()){
			srcSpace.setValue(usedSpaces.get(0));
		}
    }
    
    private void formatTextField(TextField t){		//formatiert eingabefelder auf maximal 4 stellige zahl eingaben
    	int len = 4;
		UnaryOperator<Change> modifyChange = c -> {
		    if (c.isContentChange()) {
		        int newLength = c.getControlNewText().length();
		        if (newLength > len) {
		            // replace the input text with the last len chars
		            String tail = c.getControlNewText().substring(newLength - len, newLength);
		            c.setText(tail);
		            // replace the range to complete text
		            // valid coordinates for range is in terms of old text
		            int oldLength = c.getControlText().length();
		            c.setRange(0, oldLength);
		        }
		    }
		    return c;
		};
		
		t.setTextFormatter(new TextFormatter<Change>(modifyChange));
		t.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*")) {
					t.setText(oldValue);
				}
			}
		});
    }
    
    @FXML
    void searchIdSrc(ActionEvent event) {
    	String input = search3.getText();
    	if(!input.isEmpty()){
    		FilteredList<Space> filteredData = new FilteredList<>(storage.getSpaces(), s -> {
	        	if(!s.isEmpty())
	        		return s.toString().matches("\\d+\\s\\|\\s[0]*" + input);
	        	else
	        		return false;
	        });
			
	        if(!filteredData.isEmpty()){
	        	srcSpace.setValue(filteredData.get(0));
	        }
	        else{
	    		Window stage = searchButtonIdSrc.getScene().getWindow();
	    		double posX = stage.getX() + 10;
				double posY = stage.getY() + 40;
	    		
	    		Tooltip IDLengthTip = new Tooltip("Die gesuchte ID ist nicht vergeben!");
	    		IDLengthTip.setAnchorX(posX);
	    		IDLengthTip.setAnchorY(posY);
	    		searchButtonIdSrc.setTooltip(IDLengthTip);
	    		searchButtonIdSrc.getTooltip().setAutoHide(true);
	    		searchButtonIdSrc.getTooltip().show(searchButtonIdSrc.getScene().getWindow());
	    	}
    	}
	}
    
    @FXML
    void searchIdDest(ActionEvent event) {
    	String input = search3.getText();
    	if(!input.isEmpty()){
    		FilteredList<Space> filteredData = new FilteredList<>(storage.getSpaces(), s -> {
	        	if(!s.isEmpty())
	        		return s.toString().matches("\\d+\\s\\|\\s[0]*" + input);
	        	else
	        		return false;
	        });
			
	        if(!filteredData.isEmpty()){
	        	destSpace.setValue(filteredData.get(0));
	        }
	        else{
	    		Window stage = searchButtonIdDest.getScene().getWindow();
	    		double posX = stage.getX() + 10;
				double posY = stage.getY() + 40;
	    		
	    		Tooltip IDLengthTip = new Tooltip("Die gesuchte ID ist nicht vergeben!");
	    		IDLengthTip.setAnchorX(posX);
	    		IDLengthTip.setAnchorY(posY);
	    		searchButtonIdDest.setTooltip(IDLengthTip);
	    		searchButtonIdDest.getTooltip().setAutoHide(true);
	    		searchButtonIdDest.getTooltip().show(searchButtonIdDest.getScene().getWindow());
	    	}
    	}
    }
    
    @FXML
    void searchIdDelete(ActionEvent event) {
    	String input = search2.getText();
    	if(!input.isEmpty()){
    		FilteredList<Space> filteredData = new FilteredList<>(storage.getSpaces(), s -> {
	        	if(!s.isEmpty())
	        		return s.toString().matches("\\d+\\s\\|\\s[0]*" + input);
	        	else
	        		return false;
	        });

	        if(!filteredData.isEmpty()){
	        	productsDelete.setValue(filteredData.get(0));
	        }
	        else{
	    		Window stage = searchButtonId2.getScene().getWindow();
	    		double posX = stage.getX() + 10;
				double posY = stage.getY() + 40;
	    		
	    		Tooltip IDLengthTip = new Tooltip("Die gesuchte ID ist nicht vergeben!");
	    		IDLengthTip.setAnchorX(posX);
	    		IDLengthTip.setAnchorY(posY);
	    		searchButtonId2.setTooltip(IDLengthTip);
	    		searchButtonId2.getTooltip().setAutoHide(true);
	    		searchButtonId2.getTooltip().show(searchButtonId2.getScene().getWindow());
	    	}
    	}
    }

    @FXML
    void searchIdOverview(ActionEvent event) throws SQLException, DatenbankUnerreichbarException {
    	String input = search1.getText();
    	if(searchButtonId.getText().equals("Zur�ck")){
			searchButtonId.setText("Suche ID");
			listProducts.setItems(allSpaces);
    	}
    	else if(!input.isEmpty() && searchButtonId.getText().equals("Suche ID") && !searchButtonPlace.getText().equals("Zur�ck")){
    		FilteredList<Space> filteredData = new FilteredList<>(storage.getSpaces(), s -> {
	        	if(!s.isEmpty())
	        		return s.toString().matches("\\d+\\s\\|\\s[0]*" + input);
	        	else
	        		return false;
	        });
			
    		if(!filteredData.isEmpty()){
    			searchButtonId.setText("Zur�ck");
    			listProducts.setItems(filteredData);
    		}
    		
    		else{
        		Window stage = searchButtonId.getScene().getWindow();
        		double posX = stage.getX() + 10;
    			double posY = stage.getY() + 40;
        		
        		Tooltip IDLengthTip = new Tooltip("Die gesuchte ID ist nicht vergeben!");
        		IDLengthTip.setAnchorX(posX);
        		IDLengthTip.setAnchorY(posY);
        		searchButtonId.setTooltip(IDLengthTip);
        		searchButtonId.getTooltip().setAutoHide(true);
        		searchButtonId.getTooltip().show(searchButtonId.getScene().getWindow());
        	}
    	}
    }
    
    @FXML
    void searchPlaceSrc(ActionEvent event) {
    	String input = search3.getText();
    	if(!input.isEmpty()){
    		int inputNumber = Integer.parseInt(input);
    		if(!(inputNumber > storage.getSpaces().size()) && !storage.getSpaces().get(inputNumber).isEmpty()){
		    	if(!input.isEmpty()){
		    		FilteredList<Space> filteredData = new FilteredList<>(storage.getSpaces(), s -> {
			        	if(!s.isEmpty())
			        		return s.toString().matches(input + "\\s\\|\\s\\d{1,4}");
			        	else
			        		return s.toString().matches(input + "\\s\\|\\s\\d*");
			        });
		    		
			        if(!filteredData.isEmpty()){
			        	srcSpace.setValue(filteredData.get(0));
			        }
		    	}
	    	}
	    	else{
	    		Window stage = searchButtonPlaceSrc.getScene().getWindow();
	    		double posX = stage.getX() + 10;
				double posY = stage.getY() + 40;
	    		
	    		Tooltip StorageSizeTip = new Tooltip("Der angegebene Stellplatz ist nicht belegt oder existiert nicht.");
	    		StorageSizeTip.setAnchorX(posX);
	    		StorageSizeTip.setAnchorY(posY);
	    		searchButtonPlaceSrc.setTooltip(StorageSizeTip);
	    		searchButtonPlaceSrc.getTooltip().setAutoHide(true);
	    		searchButtonPlaceSrc.getTooltip().show(stage);
	    	}
    	}
    }
    
    @FXML
    void searchPlaceDest(ActionEvent event) {
    	String input = search3.getText();
    	if(!input.isEmpty()){
    		int inputNumber = Integer.parseInt(input);
	    	if(!(inputNumber > storage.getSpaces().size())){
		    	if(!input.isEmpty()){
		    		FilteredList<Space> filteredData = new FilteredList<>(storage.getSpaces(), s -> {
			        	if(!s.isEmpty())
			        		return s.toString().matches(input + "\\s\\|\\s\\d{1,4}");
			        	else
			        		return s.toString().matches(input + "\\s\\|\\s\\d*");
			        });

			        if(!filteredData.isEmpty()){
			        	destSpace.setValue(filteredData.get(0));
			        }
		    	}
	    	}
	    	else{
	    		Window stage = searchButtonPlaceDest.getScene().getWindow();
	    		double posX = stage.getX() + 10;
				double posY = stage.getY() + 40;
	    		
	    		Tooltip StorageSizeTip = new Tooltip("Der angegebene Stellplatz existiert nicht.");
	    		StorageSizeTip.setAnchorX(posX);
	    		StorageSizeTip.setAnchorY(posY);
	    		searchButtonPlaceDest.setTooltip(StorageSizeTip);
	    		searchButtonPlaceDest.getTooltip().setAutoHide(true);
	    		searchButtonPlaceDest.getTooltip().show(stage);
	    	}
    	}
    }
    
    @FXML
    void searchPlaceDelete(ActionEvent event) {
    	String input = search2.getText();
    	if(!input.isEmpty()){
    		int inputNumber = Integer.parseInt(input);
	    	if(!(inputNumber > storage.getSpaces().size()) && !storage.getSpaces().get(inputNumber).isEmpty()){
		    	if(!input.isEmpty()){
		    		FilteredList<Space> filteredData = new FilteredList<>(storage.getSpaces(), s -> {
			        	if(!s.isEmpty())
			        		return s.toString().matches(input + "\\s\\|\\s\\d{1,4}");
			        	else
			        		return s.toString().matches(input + "\\s\\|\\s\\d*");
			        });
		    		
			        if(!filteredData.isEmpty()){
			        	productsDelete.setValue(filteredData.get(0));
			        }
		    	}
	    	}
	    	else{
	    		Window stage = searchButtonPlace.getScene().getWindow();
	    		double posX = stage.getX() + 10;
				double posY = stage.getY() + 40;
	    		
	    		Tooltip StorageSizeTip = new Tooltip("Der angegebene Stellplatz ist nicht belegt oder existiert nicht.");
	    		StorageSizeTip.setAnchorX(posX);
	    		StorageSizeTip.setAnchorY(posY);
	    		searchButtonPlace.setTooltip(StorageSizeTip);
	    		searchButtonPlace.getTooltip().setAutoHide(true);
	    		searchButtonPlace.getTooltip().show(searchButtonPlace.getScene().getWindow());
	    	}
    	}
    }
    
    @FXML
    void searchPlaceOverview(ActionEvent event) {
    	String input = search1.getText();
    	int inputNumber = Integer.parseInt(input);
    	if(searchButtonPlace.getText().equals("Zur�ck")){
			searchButtonPlace.setText("Suche Stellplatz");
			listProducts.setItems(allSpaces);
		}
    	else if(!(inputNumber > storage.getSpaces().size())){
	    	if(!input.isEmpty() && searchButtonPlace.getText().equals("Suche Stellplatz") && !searchButtonId.getText().equals("Zur�ck")){
	    		searchButtonPlace.setText("Zur�ck");
	    		FilteredList<Space> filteredData = new FilteredList<>(storage.getSpaces(), s -> {
		        	if(!s.isEmpty())
		        		return s.toString().matches(input + "\\s\\|\\s\\d{1,4}");
		        	else
		        		return s.toString().matches(input + "\\s\\|\\s\\d*");
		        });
				
		        listProducts.setItems(filteredData);
	    	}
    	}
    	else{
    		Window stage = searchButtonPlace.getScene().getWindow();
    		double posX = stage.getX() + 10;
			double posY = stage.getY() + 40;
    		
    		Tooltip StorageSizeTip = new Tooltip("Der angegebene Stellplatz ist nicht vorhanden.");
    		StorageSizeTip.setAnchorX(posX);
    		StorageSizeTip.setAnchorY(posY);
    		searchButtonPlace.setTooltip(StorageSizeTip);
    		searchButtonPlace.getTooltip().setAutoHide(true);
    		searchButtonPlace.getTooltip().show(searchButtonPlace.getScene().getWindow());
    	}
	}
}
package application;

import java.io.IOException;

import javafx.event.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

public class MainController {
	
	@FXML
	
	private Label lblStatus;
	
	@FXML
	
	private TextField txtUserName;


	@FXML

	private TextField txtPassword;
	
	public void Login(ActionEvent e) throws IOException {
		if(txtUserName.getText().equals("user") && txtPassword.getText().equals("1234")) {
			
			lblStatus.setText("Login Success");
			Stage primaryStage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("/application/MainPage.fxml"));
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} else {
			
			lblStatus.setText("Login Failed!");
		}
	}
}
		

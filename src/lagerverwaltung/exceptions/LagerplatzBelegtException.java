package lagerverwaltung.exceptions;

public class LagerplatzBelegtException extends Exception{
	public LagerplatzBelegtException(){super();}
	public LagerplatzBelegtException(String message){super(message);}
}

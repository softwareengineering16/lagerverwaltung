package lagerverwaltung.exceptions;


public class PaketSchonVorhandenException extends Exception{
	public PaketSchonVorhandenException(){super();}
	public PaketSchonVorhandenException(String message){super(message);}
}
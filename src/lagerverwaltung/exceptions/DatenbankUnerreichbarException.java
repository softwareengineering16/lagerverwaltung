package lagerverwaltung.exceptions;

public class DatenbankUnerreichbarException extends Exception{
	public DatenbankUnerreichbarException(){super();}
	public DatenbankUnerreichbarException(String message){super(message);}
}

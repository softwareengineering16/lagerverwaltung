package lagerverwaltung.exceptions;

public class LagerplatzOutOfBounceException extends Exception{
	public LagerplatzOutOfBounceException(){super();}
	public LagerplatzOutOfBounceException(String message){super(message);}
}

package lagerverwaltung.middleware;

/**
 * 
 * @author coffee
 *
 */
public class Package {
    
    private String id;
    private Space space;
    
    /**
     * 
     * @param number
     */
    public Package (String number) {
        id = number;
        space = null;
    }
    
    /**
     * 
     * @return
     */
    public String getId() {
        return id;
    }
    
    /**
     * 
     * @return
     */
    public Space getSpace() {
        return space;
    }
    
    
    /**
     * 
     * @param x
     */
    void setSpace(Space x) {
        if (x == null || isDetached())
            space = x;
    }

    /**
     * 
     * @return
     */
    public boolean isDetached()
    {
        return space == null;
    }
    
    /**
     * 
     */
    @Override
    public String toString() {
        return getId();
    }
}

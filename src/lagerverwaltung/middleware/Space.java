package lagerverwaltung.middleware;

/**
 * 
 * @author coffee
 *
 */
public class Space {
    private Package packet;
    private String number;

    /**
     * ctor - space has no packet
     */
    public Space(String number)
    {
        packet = null;
        this.number = number;
    }
    
    /**
     * 
     * @return
     */
    public String getNumber() {
        return number;
    }
    
    /**
     * returns the stored packet and removes it from space
     * @return stored packet
     */
    public Package getPacket() {
        if (isEmpty())
            return null;
        packet.setSpace(null);
        Package result = packet;
        packet = null;
        return result;
    }
    
    /**
     * returns the stored packet
     * @return stored packet
     */
    public Package watchPacket() {
        return packet;
    }

    /**
     * set packet to the space if empty
     * @param packet
     * @return
     */
    public boolean setPacket(Package packet) {
        if (isEmpty() && packet.isDetached())
        {
            packet.setSpace(this);
            this.packet = packet;
            return true;
        }
        return false;
    }
    
    /**
     * checks if space has packet or not
     * @return empty space
     */
    public boolean isEmpty() {
        return packet == null;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        return number + " | "  + (isEmpty() ? "*" : watchPacket());
    }
    
}

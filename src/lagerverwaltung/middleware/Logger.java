package lagerverwaltung.middleware;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
    
    private static String logName = "log01";
    private static boolean done = false;
    private final static String endung = ".txt";
    private final static String folder = "logs/";
    
    public static void writeLog(Exception toLog){
        
        writeLog(toLog.getMessage());
    }
    
    public static void writeLog(String toLog){
    
        logAnlegen();    
        BufferedWriter fw = null;
        
        
        
        try
        {
          fw = new BufferedWriter(new FileWriter(folder + logName + endung ,true ));
          fw.append( datum() + " " + toLog);
          fw.newLine();
        }
        catch ( IOException e ) {
          System.err.println( "Konnte Log nicht erstellen" );
        }
        finally {
          if ( fw != null )
            try { fw.close(); } catch ( IOException e ) { e.printStackTrace(); }
        }
    }
    
    private static String datum (){
       
        SimpleDateFormat formatter = new SimpleDateFormat(
                "yyyy.MM.dd - HH:mm:ss ");
        
        return formatter.format(new Date());
    }
    private static void logAnlegen(){
        if(done)
           return;
        new File(folder).mkdirs();
        File log = new File(folder + logName + endung);
        int i = 0;
        while(log.exists()){
            ++i;
            logName = "log"+ (i < 10 ? "0" + i : i );
            log = new File(folder + logName + endung);
        }
        done = true;
     }

}

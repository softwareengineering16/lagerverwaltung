package lagerverwaltung.middleware;



import java.io.File;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lagerverwaltung.backend.DBConnect;
import lagerverwaltung.backend.DBWrapper;
import lagerverwaltung.exceptions.DatenbankUnerreichbarException;
import lagerverwaltung.exceptions.LagerVollException;
import lagerverwaltung.exceptions.LagerplatzBelegtException;
import lagerverwaltung.exceptions.LagerplatzOutOfBounceException;
import lagerverwaltung.exceptions.PaketSchonVorhandenException;

/**
 * @author coffee
 *
 */
public class Storage {
    private ObservableList<Space> spaces;
    private DBConnect backend;
    
    

     /**
     * pruefen ob db existiert, wenn nich --> erstellen
     * @throws SQLException 
     */
    void dbAnlegen() throws SQLException{
        File db = new File("../backend/sample.db");
        if (!db.exists()){
            try {
                backend.initialiseDB();
            } catch (DatenbankUnerreichbarException e) {
               
            }
        }
     }

    /**
     * ctor - for testing
     */
    private Storage() {
        spaces = FXCollections.observableList(new LinkedList<Space>());
    }
    
    /**
     * 
     * @param backend
     * @throws DatenbankUnerreichbarException 
     * @throws SQLException 
     */
    public Storage(DBConnect backend) throws SQLException, DatenbankUnerreichbarException {
        this();
        setBackend(backend);
    }
    
    /**
     * 
     * @param db
     * @throws DatenbankUnerreichbarException 
     * @throws SQLException 
     */
    public void setBackend(DBConnect db) throws SQLException, DatenbankUnerreichbarException {
        backend = db;
        refresh();
    }
    
    /**
     * refresh data
     * @throws DatenbankUnerreichbarException 
     * @throws SQLException 
     */
    void refresh() throws SQLException, DatenbankUnerreichbarException {
        spaces.clear();
        List<String> allSpaces;
        allSpaces = backend.getAllPlaces();
        LinkedList<String> usedSpaces =  backend.getAllUsedPlaces();
        for (String iterSpace : allSpaces) {
            Space temp =  new Space(iterSpace);
            spaces.add(temp);
            if (usedSpaces.contains(iterSpace)) {
                try {
                    String packet = backend.getPID(iterSpace);
                    if (!"".equals(packet))
                        temp.setPacket(new Package(packet));
                } catch (LagerplatzOutOfBounceException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }  
        }
        
        System.out.println("> data refreshed");
    }

    /**
     * adds new package
     * @param sid space
     * @param pid package
     * @throws LagerVollException 
     * @throws LagerplatzOutOfBounceException 
     * @throws LagerplatzBelegtException 
     * @throws DatenbankUnerreichbarException 
     * @throws SQLException 
     */
    public void addPackage(Package pid) throws LagerVollException, LagerplatzBelegtException, LagerplatzOutOfBounceException, SQLException, DatenbankUnerreichbarException,LagerplatzBelegtException {
        Space temp = findNextFreeSpace();
        //if (!"".equals(backend.getPlaceID(pid.getId()))/* && !isIdUsed(pid.getId())*/)
        {
            backend.setNewProduct(pid.getId(), temp.getNumber());
            refresh();
        }
    }
    
    /**
     * 
     * @param pid
     * @return
     * @throws PaketSchonVorhandenException 
     */
    public Package newPackage(String pid) throws PaketSchonVorhandenException {
        //if (!isIdUsed(pid))
            return new Package(pid);
        //throw new PaketSchonVorhandenException();
    }
    
    public void destroyPackage(Package packet) throws SQLException, DatenbankUnerreichbarException {
        backend.setDelProduct(packet.getId());
        refresh();
    }
    
    public Space findNextFreeSpace() throws LagerVollException, SQLException, DatenbankUnerreichbarException {
        String space = backend.getNextBestPlace().get(0);
        for (Space iter : spaces)
        {
            if (iter.getNumber().equals(space))
                return iter;
        }
        throw new LagerVollException();        
    }
    
    /**
     * swaps content of two spaces
     * @param from
     * @param to
     */
    public void changePackage(Space from, Space to) {
        try {
            backend.setSwapPlaces(from.getNumber(), to.getNumber());
            refresh();
        } catch (SQLException | DatenbankUnerreichbarException | LagerplatzOutOfBounceException e) {
            e.printStackTrace();
        }

    }
    
    /**
     * return ObservableList for combobox
     * @return
     */
    public ObservableList<Space> getSpaces() {
        return spaces;
    }
    
    
    /**
     * 
     */
    @Override
    public String toString() {
        String result = "Storage:\n";
        for (Space iter : spaces)
            result += iter + "\n";
        return result;
    }
    
    
    public ObservableList<Space> getUsedSpaces(){
        
        ObservableList<Space> usedSpaces = FXCollections.observableList(new LinkedList<Space>());
        try {
            LinkedList<String> tmp; 
            
            tmp = backend.getAllUsedPlaces();
            
            for(String iter : tmp){
                Space space = new Space (iter);
                space.setPacket(new Package(backend.getPID(iter)));
                usedSpaces.add(space);
                 
             }
            
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DatenbankUnerreichbarException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (LagerplatzOutOfBounceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
       
      

        
        return usedSpaces;
    }

    /**
     * quicktest
     * @param args
     * @throws DatenbankUnerreichbarException 
     * @throws SQLException 
     */
    public static void main(String[] args) throws DatenbankUnerreichbarException, SQLException {
        Storage test;
        try {
            test = new Storage(new DBWrapper());
            System.out.println(test);
        } catch (DatenbankUnerreichbarException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
       
    }
    
    
    
}
